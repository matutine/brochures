# Brochures #

Brochures provenant  de différentes  sources, proposées  en différents
formats (notamment  le format livret dont  une feuille sur deux  a été
pivotée à  180°, afin de faire  face à une imprimante  récalcitrante à
l'université ou au bureau).

## Acrimed - Action Critique Médias

Acrimed (Action Critique Médias) est  une association de critique des
médias.

http://www.acrimed.org/

«Née  du mouvement  social de  1995,  dans la  foulée de  l’Appel à  la
solidarité  avec les  grévistes, notre  association, pour  remplir les
fonctions  d’un observatoire  des médias  s’est constituée,  depuis sa
création  en 1996,  comme une  association-carrefour. Elle  réunit des
journalistes et salariés des médias, des chercheurs et universitaires,
des acteurs  du mouvement social et  des « usagers »  des médias. Elle
cherche à mettre en  commun savoirs professionnels, savoirs théoriques
et savoirs militants au  service d’une critique indépendante, radicale
et intransigeante.»

Brochures:

- les médias et le Front National


## Lyberagones - textes des revues Agone ##

La revue Agone fut un temps en  lecture libre sur le site des éditions
Agone: <http://agone.org/revueagone>

### Propagande et controle de l'esprit public, Noam Chomsky (lyberagone n°34) ###

On ne présente pas Chomsky !

## Renseignements Généreux ##

Leurs brochures sont là:
<http://www.les-renseignements-genereux.org/brochures/>

nous ajoutons seulement le format brochure  dont une page sur deux est
retournée.

## Normand Baillargeon ##

### petit cours d'autodéfense intellectuelle. ###

Normand Baillargeon est professeur en sciences de l'éducation à
l'université du Québec à Montréal, essayiste, collaborateur de
différentes revues, libertaire…

<https://fr.wikipedia.org/wiki/Normand_Baillargeon>

Cette brochure a été augmentée et enrichie et a aboutie à la
publication d'un livre, chez Lux éditeurs (Québec). Donc si vous aimez
la brochure, achetez et offrez le livre !

![Petit cours d'autodéfense intellectuelle](http://www.luxediteur.com/sites/lux.aegirphp52.koumbit.net/files/autodefense.jpg)


Il a écrit d'autres supers chouettes livres, tels que

* L'ordre moins le pouvoir, histoire et actualité de l'anarchisme, éd. Agone <http://agone.org/elements/lordremoinslepouvoir/>

![](http://agone.org/couverture/couv_1523.png)

* Les chiens ont soif. Critiques et propositions libertaires, éd. Lux
  (médias, esprit critique,…) <http://www.luxediteur.com/content/les-chiens-ont-soif>

  ![](http://www.luxediteur.com/sites/lux.aegirphp52.koumbit.net/files/couv-chiens-ont-soif-site.jpg)

* Là-haut, il n'y a rien. Anthologie de l’incroyance et de la libre-pensée, éd. Laval <https://www.pulaval.com/produit/la-haut-il-n-y-a-rien-anthologie-de-l-incroyance-et-de-la-libre-pensee>

   ![](https://www.pulaval.com/media/books/thumb_L97827637876191.jpg)

* etc !

Il intervient dans le film Chomsky et cie de Daniel Mermet.[Chomsky et cie][]

[Chomsky et cie]: https://thepiratebay.se/torrent/4379494/Chomsky___Compagnie "film torrent"

### Propaganda, introduction au livre d'Edward Bernays ##
