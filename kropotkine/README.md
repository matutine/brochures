Aux jeunes gens, de Pierre Kropotkine, paru en 1904 dans les Temps Nouveaux.



On peut trouver un pdf sur les sites:

* <http://www.la-presse-anarchiste.net/spip.php?rubrique312>
* <https://www.marxists.org/francais/general/kropotkine/1904/00/jeunes_gens.htm> (ainsi qu'un fichier au format rtf)

J'ai utilisé le texte du second lien pour créer le fichier latex et
générer le pdf associé.

J'ai pris le fichier rtf pour créer le fichier odt, auquel j'ai
rajouté l'entête et les numéros de page. C'est à partir de celui-ci
que j'ai créé le livret (impossible avec mon document latex...).

On peut également trouver un petit livre à 4€
[aux éditions D'Ores et Déjà](http://www.doresetdeja.fr/kropotkine.html).

![](http://www.doresetdeja.fr/10kropotkine.jpg)

Publié dans le journal anarchiste Les temps nouveaux en 1904, le texte
Aux jeunes gens s'adresse à un public en devenir, des hommes et des
femmes achevant leurs études, précisera l'auteur lui-même, Pierre
Kropotkine. Alliant suggestions, conseils et avertissements, le
théoricien anarcho-communiste tente ici d'éveiller les esprits les
plus jeunes à l'idée de justice sociale, les incitant à oeuvrer en vue
d'une humanité meilleure.

Captivant, sans oublier d'être drôle, Aux jeunes gens pourrait être lu
à la manière de la Lettre à un jeune poète de Rainer Maria Rilke,
publiée bien plus tard, en 1929. Les conseils de Kropotkine, à
destination d'un lectorat éloigné des préoccupations politiques et
sociales du début du XXe siècle, ne s'encombrent pas
d'euphémismes. Identifiant les affres présents et à venir, nommant les
maux d'une société avide, l'auteur presse ses lecteurs d'agir, ici et
maintenant.

Aux jeunes gens s'inscrit parfaitement dans l'oeuvre globale de Pierre
Kropotkine, tout en participant à l'indispensable transmission chère
aux militants politiques désireux de perpétuer la pensée de leur
action.

Parmi les ouvrages essentiels de Kropotkine, il convient de citer La
conquête du pain (1892), texte influençant considérablement la pensée
anarchiste, puis L'entraide, un facteur de l'évolution (1902). On
trouve ici le socle de la pensée de Pierre Kropotkine, une théorie
basée sur le caractère indispensable des principes d'entraide et de
coopération, sans lesquels la nature humaine tomberait inexorablement
dans la hiérarchisation sociale et le despotisme. Quelques années
avant sa mort, toujours dans Les temps nouveaux, Kropotkine publie
L'esprit de révolte, interrogation profonde et essentielle sur le
moyen de faire passer un peuple d'une situation de profonde
indignation à celle d'une insurrection.

Fabrice Millon
