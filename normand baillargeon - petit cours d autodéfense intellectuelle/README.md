Normand Baillargeon est professeur en sciences de l'éducation à
l'université du Québec à Montréal, essayiste, collaborateur de
différentes revues, libertaire…

<https://fr.wikipedia.org/wiki/Normand_Baillargeon>

Cette brochure a été augmentée et enrichie et a aboutie à la
publication d'un livre, chez Lux éditeurs (Québec). Donc si vous aimez
la brochure, achetez et offrez le livre !

![Petit cours d'autodéfense intellectuelle](http://www.luxediteur.com/sites/lux.aegirphp52.koumbit.net/files/autodefense.jpg)


Il a écrit d'autres supers chouettes livres, tels que

* L'ordre moins le pouvoir, histoire et actualité de l'anarchisme, éd. Agone <http://agone.org/elements/lordremoinslepouvoir/>

![](http://agone.org/couverture/couv_1523.png)

* Les chiens ont soif. Critiques et propositions libertaires, éd. Lux
  (médias, esprit critique,…) <http://www.luxediteur.com/content/les-chiens-ont-soif>
  ![](http://www.luxediteur.com/sites/lux.aegirphp52.koumbit.net/files/couv-chiens-ont-soif-site.jpg)

* Là-haut, il n'y a rien. Anthologie de l’incroyance et de la libre-pensée, éd. Laval <https://www.pulaval.com/produit/la-haut-il-n-y-a-rien-anthologie-de-l-incroyance-et-de-la-libre-pensee>
   ![](https://www.pulaval.com/media/books/thumb_L97827637876191.jpg)

* etc !
