#!/bin/bash

# Usage :
# source pdf2booklet.sh entree.pdf [final.pdf]
# Les noms de fichiers ne doivent pas contenir d'espaces.

# TODO: argument manquant.

# Ce script devrait être accessible via un clic droit dans Nautilus:
# remplacer $1 par NAUTILUS_SCRIPT_SELECTED_FILE_PATHS
# et d'utiliser un peu zenity/son fork pour les boites de dialogue.

file=$1
final=$2
ad=$(pwd)

suffix="-livret"
if (($# = 1)) ;then
    final=`basename $1 .pdf`
    final=${final}${suffix}.pdf;
fi;
echo -n "le fichier de sortie s'appelle "; echo $final;

# 1ere etape : mise en page des pages en positionnement livret
# et format a5 du document pdf de depart.
echo "lancement de pdf2ps sur $file "
pdf2ps $file depart.ps
psbook  depart.ps book.ps
psnup -l -Pa4 -n 2 book.ps paysage.ps
echo "lancement de ps2pdf..."
ps2pdf paysage.ps to_rotate.pdf
rm depart.ps book.ps paysage.ps

if ! [ -d .faire_livret/ ] ; then
    mkdir .faire_livret/
fi
echo "pdftk dans le dossier .faire_livret/"

# Split the pdf.
cd .faire_livret/
pdftk ../to_rotate.pdf burst output
rm ../to_rotate.pdf
# rm *.txt #fichier de rapport créé par pdftk

# Rotate one page out of two.
echo "selection des pages paires ..."
sleep 2
i=0;
# We may not need this for: learn pdftk !
for fich in ./*.pdf; do
    if ((i==0)); then  ##double parentheses => syntaxe "à la C"
	i=1
    else
	pdf90 $fich --outfile tmp.pdf
	rm $fich
	pdf90 tmp.pdf --outfile $fich #tourner 2 fois : tourner de 180°
	rm tmp.pdf
	i=0
    fi;

done;

# Join everything.
echo "lancement de pdfjoin ..."
pdfjoin *.pdf --outfile ../$final
cd $ad
rm -r .faire_livret/
echo "Manipulations terminees. Verifier !"
